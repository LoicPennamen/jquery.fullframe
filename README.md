# JQuery.fullFrame

JQuery plugin to stretch an iFrame content to its optimal dimensions, while keeping its ratio.  
This script is to be put inside the included document.  

## How to use

Like this inside your included page - *not* the page containing the <iframe> element:

```html

	<!-- You need jquery -->
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="js/jquery.fullFrame.js"></script>
	
	<!-- Replace -->
	<script type="text/javascript">
		$( document ).ready(function() {
			// Replace my_animation by your target element
			$('#my_animation').fullFrame();
		});
	</script>
	
```